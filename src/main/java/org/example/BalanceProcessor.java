package org.example;

import java.util.ArrayList;
import java.util.List;

public class BalanceProcessor {

    public List<Double> calculateBalance(List<Product> itemList, List<PromotedProduct> promotedProducts) {
        List<Double> balanceList = new ArrayList<>();
        if (!promotedProducts.isEmpty()) {
            balanceList.add(itemList.stream().mapToDouble(Product::getSubTotal).sum()-promotedProducts.stream().mapToDouble(PromotedProduct::getValue).sum());
            balanceList.add(promotedProducts.stream().mapToDouble(PromotedProduct::getValue).sum());
        }else {
            balanceList.add(itemList.stream().mapToDouble(Product::getSubTotal).sum());
        }
        return balanceList;
    }
}
