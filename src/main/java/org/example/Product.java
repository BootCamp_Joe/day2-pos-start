package org.example;

import lombok.*;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Getter
@Setter
public class Product {
    private String name;
    private String quantity;
    private double unitPrice;
    private double subTotal;

    @Override
    public String toString() {
        return String.format("Name: %s, Quantity: %s, Unit Price: %,.2f (CNY), Subtotal: %,.2f (CNY)", name, quantity, unitPrice, subTotal);
    }


}
