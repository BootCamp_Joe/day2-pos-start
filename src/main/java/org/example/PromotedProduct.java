package org.example;

import lombok.*;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Setter
@Getter
public class PromotedProduct {
    private String name;
    private String quantity;
    private double value;

    @Override
    public String toString() {
        return String.format("Name: %s, Quantity: %s, Value: %,.2f (CNY)", name, quantity, value);
    }
}
