package org.example;

import java.util.List;
import java.util.Map;

public class ReceiptGenerator {

    public String generateReceipt(List<String> items){
        Map<String, String> allItems = PosDataLoader.loadAllItems();
        String[] allPromotions = PosDataLoader.loadPromotion();
        ShoppingItemsProcessor shoppingItemsProcessor = new ShoppingItemsProcessor();
        PromotionItemsProcessor promotionItemsProcessor = new PromotionItemsProcessor();
        BalanceProcessor balanceProcessor = new BalanceProcessor();
        Printer printer = new Printer();

        List<Product> itemList = shoppingItemsProcessor.processShoppingList(allItems, items);
        List<PromotedProduct> promotedProducts =  promotionItemsProcessor.processPromotionList(itemList, promotionItemsProcessor.convertProductNameFromPromotionCode(allPromotions, allItems));
        List<Double> balanceList = balanceProcessor.calculateBalance(itemList, promotedProducts);
        return printer.print(itemList, promotedProducts, balanceList);
    }


}
