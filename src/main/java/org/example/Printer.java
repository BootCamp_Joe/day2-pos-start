package org.example;


import java.util.List;

public class Printer {
    public String print(List<Product> itemList,  List<PromotedProduct> promotionList, List<Double> balanceList) {

        return "***<No Profit Store> Shopping List***\n" +
          "----------------------\n" +
          processItemList(itemList) +
          "----------------------\n" +
          "Buy three get one free items：\n" +
          processPromotionist(promotionList) +
          "----------------------\n" +
          processBalanceList(balanceList) +
          "**********************\n";
    }

    public String processItemList (List<Product> itemList) {
        StringBuilder stringBuilder = new StringBuilder();
        for(Product item: itemList) {
            stringBuilder.append(item.toString() + "\n");
        }
        return stringBuilder.toString();
    }

    public String processPromotionist (List<PromotedProduct> promotionList) {
        StringBuilder stringBuilder = new StringBuilder();
        for(PromotedProduct promotedProduct: promotionList) {
            stringBuilder.append(promotedProduct.toString() + "\n");
        }
        return stringBuilder.toString();
    }

    public String processBalanceList (List<Double> balanceList) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Total：%.2f(CNY)\n", balanceList.get(0)));
        if (balanceList.size() == 2) {
            stringBuilder.append(String.format("Saved：%.2f(CNY)\n", balanceList.get(1)));
        }
        return stringBuilder.toString();
    }
}

