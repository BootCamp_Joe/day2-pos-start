package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PromotionItemsProcessor {

    public List<PromotedProduct> processPromotionList(List<Product> itemList, List<String> promotionList) {
        List<PromotedProduct> promotedProducts = new ArrayList<>();
        for(Product product: itemList) {
            if(promotionList.contains(product.getName())) {
                String[] quantityUnit = product.getQuantity().split(" ");
                int free = Integer.parseInt(quantityUnit[0])/3;
                if (free >= 1) {
                    System.out.println(quantityUnit[0]);
                    promotedProducts.add(PromotedProduct.builder()
                            .name(product.getName())
                            .quantity(String.format("%d %s", free, free > 1? quantityUnit[1] : quantityUnit[1].substring(0, quantityUnit[1].length() - 1)))
                            .value(free * product.getUnitPrice())
                            .build());
                }

            }
        }
        return promotedProducts;
    }

    public List<String> convertProductNameFromPromotionCode (String[] promotionCode, Map<String, String> itemList) {
        List<String> productName = new ArrayList<>();
        for(String code: promotionCode) {
            productName.add(itemList.get(code).split(",")[0]);
        }
        return productName;
    }

}
