package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingItemsProcessor {

    public List<Product> processShoppingList(Map<String, String> itemList, List<String> shoppingList) {
        Map<String, Double> itemCount = countShoppingItems(shoppingList);
        return getShoppingItems(itemCount, itemList);
    }

    public Map<String, Double> countShoppingItems(List<String> shoppingList) {
        Map<String, Double> itemCount = new HashMap<>();
        for(String code: shoppingList) {
            double quantity;
            String[] codeQuantity = code.split("-");
            quantity = codeQuantity.length == 2? Double.parseDouble(codeQuantity[1]) : 1.0;

            if(!itemCount.containsKey(codeQuantity[0])) {
                itemCount.put(codeQuantity[0], quantity);
            }else {
                itemCount.put(codeQuantity[0], itemCount.get(codeQuantity[0]) + quantity);
            }
        }
        return itemCount;
    }

    public List<Product> getShoppingItems(Map<String, Double> itemCount, Map<String, String> itemList) {
        List<Product> shoppingItems = new ArrayList<>();
        for(String code: itemCount.keySet()) {
            String[] productInfo = itemList.get(code).split(",");
            double quantity = itemCount.get(code);
            String quantityStr = quantity % 1 == 0? String.format("%.0f ", quantity) : String.format("%.1f ", quantity);
            quantityStr = quantityStr.concat(quantity > 1? productInfo[2] + "s": productInfo[2]);
            shoppingItems.add(Product.builder()
                            .name(productInfo[0])
                            .quantity(quantityStr)
                            .unitPrice(Double.parseDouble(productInfo[1]))
                            .subTotal(quantity * Double.parseDouble(productInfo[1]))
                    .build());
        }
        return shoppingItems;
    }

}
